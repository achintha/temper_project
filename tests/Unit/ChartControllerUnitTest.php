<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ChartControllerUnitTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * to test if the landing page is correct
     */
    public function test_main_landing_page_exists()
    {
        $this->get('/')
            ->assertSeeText("Charts")
            ->assertStatus(200);
    }

    public function test_weekly_data_route_exists()
    {
        $this->get('/weekly_data')
            ->assertStatus(200);
    }

    public function test_json_returns_retention_values()
    {
        $chartConfig = json_decode($this->get('/weekly_data')->original['data'],true);
        $this->assertArrayHasKey('series',$chartConfig);
    }

    public function test_json_retention_data_starts_with_zero_percent_time()
    {
        $chartConfig = json_decode($this->get('/weekly_data')->original['data'],true);
        $this->assertEquals('0',$chartConfig['series'][0]['data'][0][0]);
    }

    public function test_json_retention_data_starts_with_hundred_percent_retention()
    {
        $chartConfig = json_decode($this->get('/weekly_data')->original['data'],true);
        $this->assertEquals('100',$chartConfig['series'][0]['data'][0][1]);
    }



}
