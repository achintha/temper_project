<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function getCharts()
    {
        return view('charts');
    }

    public function getWeeklyRetentionChart() : array
    {
        $mainArr['chart'] = ['type' => 'spline', 'inverted' => false];
        $mainArr['tooltips'] = ['enabled' => false];
        $mainArr['title'] = ['text' => 'Weekly Retention Curves'];
        $mainArr['xAxis'] = [
            'categories' => [100,80,60,40,20,0]
        ];
        $mainArr['yAxis'] = ['title' => ['text' => 'Percentage of users']];

        $mainArr['series'] = $this->getWeeklyRetentionData();

        return [
            'err' =>false,
            'data'=>json_encode($mainArr)
        ];
    }

    public function getWeeklyRetentionData() : array
    {
        $onBoardingData = \DB::table('user_onboarding_info')->get();

        $weeklyCounts = $this->calculateWeeklyCounts($onBoardingData->toArray());

        $weeklyRetentions = $this->calculateWeeklyRetentions($weeklyCounts);

        $formattedData = $this->formatWeeklyRetentions($weeklyRetentions);

        return $formattedData;
    }

    public function calculateWeeklyCounts(array $rawData) : array
    {
        $data = [];

        foreach($rawData as $row) {
            $date = (new Carbon($row->created_at))->startOfWeek()->format('Y-m-d');
            $data[$date][$row->onboarding_percentage] =
                isset($data[$date][$row->onboarding_percentage]) ? ++$data[$date][$row->onboarding_percentage] : 1;
        }

        return $data;
    }


    public function calculateWeeklyRetentions(array $rawData) : array
    {
        foreach ($rawData as $week => $countArr) {
            $tempArr = [];
            $checkedTotal = 0;
            $weekTotal = array_sum($countArr);
            ksort($countArr);

            $tempArr[0] = 100;
            foreach ($countArr as $percentage => $count) {
                $tempArr[$percentage] = round(100-($checkedTotal/$weekTotal*100),2);
                $checkedTotal += $count;
            }
            $rawData[$week] = $tempArr;
        }

        return $rawData;

    }

    public function formatWeeklyRetentions(array $rawData) : array
    {
        $formattedArray = [];

        foreach($rawData as $week => $countArr){
            $tempArr['name'] = $week;
            foreach ($countArr as $percentage => $count) {
                $tempArr['data'][] = [$percentage,$count];
            }
            $formattedArray[] = $tempArr;
            unset($tempArr);
        }

        return $formattedArray;
    }
}