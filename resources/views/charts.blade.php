<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Charts</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{{ asset('css/custom.css') }}}" rel="stylesheet" type="text/css">

    <!-- Scrips -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="{{{ asset('js/custom.js') }}}"></script>

</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div id="weekly_retention" style="width:500px; height:400px;"></div>
    </div>
</div>
</body>
</html>
