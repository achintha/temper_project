$(document).ready(function($){
    $.ajax({
        url: "/weekly_data",
        context: document.body
    }).done(function(result) {
        if(result.err == false) {
            $('#weekly_retention').highcharts(JSON.parse(result.data));
        }
    });
});