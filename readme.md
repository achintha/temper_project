## Steps to setup assignment

### configuration

1. change your [Homestead.yaml](#) file to include the following block to sites

        - map: <<local_url_tobe_used>>
    
          to: <<mapped path based on the defined folder in Homestead.yaml file>>
2. Add the database name to the databases section of [Homestead.yaml](#) 
3. Run `vagrant up --provision` from your homestead folder.
4. Add the <<local_url_tobe_used>> into `/etc/hosts` file pointing to ip specified in you [Homestead.yaml](#).

### setup application
1. Clone the repo [temper_project](https://achintha@bitbucket.org/achintha/temper_project.git) into your server.
2. ssh into your server and update the composer.
3. run `cp .env.example .env` to get a copy of the current set of environment variables.
4. change `DB_DATABASE` to the Database name you specified on [Homestead.yaml](#)
5. Run following commands

        php artisan migrate:install
        php artisan migrate
        php artisan db:seed
         
6. The chart will can be viewed from the above set URL now.