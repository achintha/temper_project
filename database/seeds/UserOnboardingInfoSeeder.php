<?php

use Illuminate\Database\Seeder;

use App\Http\Requests;
use League\Csv\Reader;

class UserOnboardingInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = public_path().'/csv/export.csv';
        $csv = Reader::createFromPath($file);
        $dataArr = $csv->setOffset(1)->fetchAll();
        foreach($dataArr as $data) {
            $row = explode(";",$data[0]);
            \DB::table('user_onboarding_info')->insert(
                array(
                    'user_id' => $row[0],
                    'created_at' => $row[1],
                    'onboarding_percentage' => !empty($row[2])?$row[2]:0,
                    'count_applications' => !empty($row[3])?$row[3]:0,
                )
            );
        }
    }
}
