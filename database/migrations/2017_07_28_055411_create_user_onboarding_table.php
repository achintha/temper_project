<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserOnboardingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_onboarding_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('onboarding_percentage');
            $table->integer('count_applications')->default(0);
            $table->integer('count_accepted_applications')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_onboarding_info');
    }
}
